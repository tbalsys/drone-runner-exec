FROM alpine

RUN wget -O- https://github.com/drone-runners/drone-runner-exec/releases/latest/download/drone_runner_exec_linux_amd64.tar.gz | tar xz -C /usr/local/bin

CMD ["drone-runner-exec"]

